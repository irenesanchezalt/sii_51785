#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>

int main(){

	
	DatosMemCompartida *pdatos;
	int fd;
	char* org;
	float mitadRaqueta;
	

	fd=open("/home/irene/DatosMemComp",O_RDWR); // SE ABRE EL FICHERO

	if(fd==-1){
		perror("Error al abrir el fichero de la memoria compartida\n");
		exit(1);
	}


	org=(char*)mmap(NULL, sizeof(*(pdatos)), PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0);

	if (org==MAP_FAILED)
	{

		perror("Error en la proyeccion del fichero origen");
		close(fd);
		exit(1);
	}	


	close(fd);
	
	pdatos=(DatosMemCompartida*)org;
	
	for(;;){

		if(pdatos->accion==3)break;
		mitadRaqueta=((pdatos->raqueta1.y1+ pdatos->raqueta1.y2)/2);
		if(mitadRaqueta < (pdatos->esfera.centro.y))
			pdatos->accion=1; //Arriba

		else if(mitadRaqueta > (pdatos->esfera.centro.y))
			pdatos->accion=-1; //Abajo				

		else pdatos->accion=0; //Nada

		usleep(2500);


	}

	munmap(org,sizeof(*(pdatos)));
	
    
}	





