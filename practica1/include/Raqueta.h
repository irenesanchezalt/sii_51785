// Raqueta.h: interface for the Raqueta class.
// MODIFICACION DE CABECERA IRENE
//////////////////////////////////////////////////////////////////////
#pragma once
#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;
        Vector2D posicion;
        Vector2D aceleracion;
	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
