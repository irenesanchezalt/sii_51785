// Mundo.h: interface for the CMundo class.
//MODIFICACION DE LA CABECERA
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include<sys/stat.h>


#define BUFFER 150 //creacion de un buffer

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	int fd; //descriptor de fichero
	char tam_buf[BUFFER]; //AÑADADO BUFFER
	
	int fd_datos;
	DatosMemCompartida datos;
	DatosMemCompartida *pdatos;

	char *org;

 
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
